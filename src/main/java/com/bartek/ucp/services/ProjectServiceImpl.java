package com.bartek.ucp.services;

import com.bartek.ucp.dao.ProjectDao;
import com.bartek.ucp.dao.UserDao;
import com.bartek.ucp.model.Project;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(value = "projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectDao projectDao;

    private static final Logger LOG = Logger.getLogger(ProjectServiceImpl.class);
//    private List<Project> projectList = new ArrayList<>();


    @Override
    public boolean projectExists(String projectName) {
//        LOG.debug("Checking if project : " + projectName + " exists.");
//        return projectList.stream().
//                filter(project -> project.getProjectName().equalsIgnoreCase(projectName)).
//                count() > 0;
        return false;
    }

    @Override
    public boolean addProject(Project newProject) {
//        LOG.debug("Add new project : " + newProject + " .");
//        projectList.add(newProject);
        projectDao.addProject(newProject);
        return true;
    }

    @Override
    public boolean editProject(Long id) {
        //TODO
        return false;
    }

    @Override
    public boolean deleteProject(Long id) {
//        LOG.debug("Deleting project" + id + " .");
//        projectList.remove(Math.toIntExact(id));
        return true;
    }

    @Override
    public List<Project> getAllProjects() {
//        LOG.debug("Getting all projects.");
//        return projectList;
        return null;
    }
}
