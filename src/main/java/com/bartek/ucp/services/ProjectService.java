package com.bartek.ucp.services;

import com.bartek.ucp.model.Project;

import java.util.List;

public interface ProjectService {

    boolean projectExists(String projectName);

    boolean addProject(Project newProject);

    boolean editProject(Long id);

    boolean deleteProject(Long id);

    List<Project> getAllProjects();


}
