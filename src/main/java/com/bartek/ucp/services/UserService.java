package com.bartek.ucp.services;

import com.bartek.ucp.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UserService {

    boolean userExists(String login);

    boolean registerUser(User userToRegister);

    List<User> getAllUsers();

    boolean userLogin(String login, String passwordHash);

    Optional<User> getUserWithId(Long id);

}
