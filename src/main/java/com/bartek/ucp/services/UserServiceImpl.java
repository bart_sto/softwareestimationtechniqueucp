package com.bartek.ucp.services;

import com.bartek.ucp.dao.UserDao;
import com.bartek.ucp.model.Project;
import com.bartek.ucp.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    ProjectService projectService;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
//    private List<User> userList = new ArrayList<>();

    @Override
    public boolean userExists(String login) {
//        LOG.debug("Checking if user : " + login + " exists.");
//        return userList.stream().
//                filter(user -> user.getLogin().equalsIgnoreCase(login)).
//                count() > 0;
        return false;
    }

    @Override
    public boolean registerUser(User userToRegister) {
//        LOG.debug("Register user : " + userToRegister + " .");
//        userList.add(userToRegister);
        userDao.addUser(userToRegister);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
//        LOG.debug("Getting all users.");
//        return userList;
        return null;
    }

    @Override
    public boolean userLogin(String login, String passwordHash) {
        //TODO do zrobienia logowanie uzytkownikow
        throw new NotImplementedException();
        //return false;
    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }


}
