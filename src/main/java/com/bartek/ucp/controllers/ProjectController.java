package com.bartek.ucp.controllers;

import com.bartek.ucp.dao.UserDao;
import com.bartek.ucp.model.Project;
import com.bartek.ucp.model.User;
import com.bartek.ucp.model.responses.Response;
import com.bartek.ucp.model.responses.ResponseFactory;
import com.bartek.ucp.services.ProjectService;
import com.bartek.ucp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.apache.log4j.Logger;

import java.time.LocalDateTime;

@RestController
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/testProject", method = RequestMethod.GET)
    public ResponseEntity<Project> requestTestProject() {
        return new ResponseEntity<Project>(
                new Project(1L, "Nazwa", "Firma", null, null),
                HttpStatus.OK) ;
    }

    @RequestMapping(value = "/addProject", method = RequestMethod.GET)
    public ResponseEntity<Response> addProject(@RequestParam String projectName,
                                                      @RequestParam String companyName,
                                                      @RequestParam LocalDateTime beginningDate,
                                                      @RequestParam Long author){
        Logger.getLogger(getClass()).debug("Requested addProject wiht params: " + projectName + " : " + companyName);

        if(projectService.projectExists(projectName)) {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Project Exists"),
                    HttpStatus.BAD_REQUEST);
        } else {
            projectService.addProject(new Project(projectName, companyName, beginningDate, author ));
            return new ResponseEntity<Response>(
                    ResponseFactory.success(),
                    HttpStatus.OK);
        }

    }

}
