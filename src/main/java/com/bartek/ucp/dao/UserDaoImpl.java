package com.bartek.ucp.dao;

import com.bartek.ucp.model.Project;
import com.bartek.ucp.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {
    @Override
    public void addUser(User u) {
        persist(u);
    }

    @Override
    public Optional<User> findById(Long id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id", id));

        return Optional.ofNullable((User) criteria.uniqueResult());
    }

    @Override
    public boolean userExists(String withLogin) {
        long resultCount = (long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", withLogin).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public void persistUser(User user) {
        persist(user);
    }
}
