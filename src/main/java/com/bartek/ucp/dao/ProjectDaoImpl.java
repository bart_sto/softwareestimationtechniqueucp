package com.bartek.ucp.dao;

import com.bartek.ucp.model.Project;
import org.springframework.stereotype.Repository;

@Repository(value = "projectDao")
public class ProjectDaoImpl extends AbstractDao implements ProjectDao {

    @Override
    public void addProject(Project p) {
        persist(p);
    }

    @Override
    public void persistProject(Project project) {
        persist(project);
    }
}
