package com.bartek.ucp.dao;

import com.bartek.ucp.model.Project;
import com.sun.istack.internal.NotNull;

public interface ProjectDao {

    void addProject(Project p);


    void persistProject(@NotNull Project project);

}
