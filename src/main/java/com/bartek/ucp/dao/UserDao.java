package com.bartek.ucp.dao;

import com.bartek.ucp.model.Project;
import com.bartek.ucp.model.User;
import com.sun.istack.internal.NotNull;

import java.util.Optional;

public interface UserDao {

    void addUser(User u);

    Optional<User> findById(@NotNull Long id);

    boolean userExists(@NotNull String withLogin);

    void persistUser(@NotNull User user);

}
