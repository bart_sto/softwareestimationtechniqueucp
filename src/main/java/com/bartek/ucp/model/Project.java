package com.bartek.ucp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="project")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project {

    @Id
    @Column
    @GeneratedValue
    private long id;

    @Column
    private String projectName;

    @Column
    private String companyName;

    @Column
    private LocalDateTime beginningDate;

    @ManyToOne
    private User author;


    public Project(String projectName, String companyName, LocalDateTime beginningDate, Long authorId) {
    }

    public Project(long id, String projectName, String companyName, LocalDateTime beginningDate, User author) {
        this.id = id;
        this.projectName = projectName;
        this.companyName = companyName;
        this.beginningDate = beginningDate;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public LocalDateTime getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(LocalDateTime beginningDate) {
        this.beginningDate = beginningDate;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
